<?php
/**
 * @file
 * paragraphs_accordion.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function paragraphs_accordion_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_paragraphs_info().
 */
function paragraphs_accordion_paragraphs_info() {
  $items = array(
    'accordion' => array(
      'name' => 'Accordion',
      'bundle' => 'accordion',
      'locked' => '1',
    ),
    'accordion_item' => array(
      'name' => 'Accordion Item',
      'bundle' => 'accordion_item',
      'locked' => '1',
    ),
  );
  return $items;
}
